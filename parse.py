import argparse
import csv
import pathlib
import re
import xml.etree.ElementTree

from inaction_t1 import S1_INACTION as T1S1, S2_INACTION as T1S2, S3_INACTION as T1S3, R1_INACTION as T1R1, R2_INACTION as T1R2, R3_INACTION as T1R3
from inaction_t2 import S1_INACTION as T2S1, S2_INACTION as T2S2, S3_INACTION as T2S3, R1_INACTION as T2R1, R2_INACTION as T2R2, R3_INACTION as T2R3

# DISCLAIMER : DO NOT INPUT UNKNOWN FILES TO THIS PROGRAM, FOR THE PYTHON XML LIBRARY USED PRESENTS SECURITY VULNERABILITIES.
# SEE https://docs.python.org/3/library/xml.html#xml-vulnerabilities FOR MORE DETAILS.
# THE AUTHOR IS NOT LIABLE FOR ANY DAMAGE CAUSED BY USAGE OF THIS SCRIPT.

MAP_SIDELENGTH = 40
MAP_SIZE = MAP_SIDELENGTH ** 2

# Circle equation. Default to a tolerance of 2 when using raw coordinates since these are meant to be quite precise
VICINITY = lambda cx, cy, tx, ty, tol = 2 : (int(cx) - tx) ** 2 + (int(cy) - ty) ** 2 <= tol ** 2
TIME = lambda t : int(t[:2]) * 3600 + int(t[3:5]) * 60 + int(t[6:])
COORDS = lambda c : (ord(c[0]) - ord('A') + 1) * (26 + (ord(c[1]) - ord('A') + 1) // (ord(c[0]) - ord('A') + 1) if len(c) == 2 else 1)

# Maximum number of seconds before considering an acknowledgement did not generate an moving intent / an actual movement
ACK_INTENT_TIMEOUT = 30
ACK_MOVEMENT_TIMEOUT = 60

# Minimum number of seconds between similar positions before a unit is considered to be in a new location
ALREADY_HERE_INTERVAL = 60

SAFE_TILESTATES = ("Clear", "ClosedOut", "BurnedOut")

# Best practice would be to attempt to parse unit states from c3fire-{nr}.log <LogEventInfo> xml tag
# However it is so incoherently formatted by C3Fire that we are better off hardcoding the values here
UNIT_STATE_IDs = {'0' : "Not Defined",
                  '1' : "Inactive",
                  '2' : "Moving",
                  '3' : "Mobilizing",
                  '4' : "Fire Fighting",
                  '5' : "De Mobilizing",
                  '6' : "Refill Water",
                  '7' : "Refill Fuel",
                  '8' : "Tap Water",
                  '9' : "Tap Fule",
                  '10' : "Creating Fire Break",
                  '11' : "Destroy",
                  '12' : "Hover",
                  '23' : "Refill People",
                  '24' : "Tap People"}

# Locations mapping in (x,y,r) format where r is the approximate radius of the city around center point x,y
LOCATIONS = {"Norwood" : (17,6,6),
             "Charville" : (23,3,4),
             "Tinderville" : (35,6,5),
             "Smokewood" : (5,12,5),
             "Woodville" : (19,11,5),
             "Smokeham" : (4,15,4),
             "Eastwood" : (34,17,9),
             "Campwood" : (13,27,7),
             "Ashtead" : (23,30,4),
             "Southwood" : (29,30,6),
             "Campville" : (11,35,5),
             "Burnham" : (36,36,4)}

RESOURCELESS_UNITS = ("R6", "R7", "R8", "R9") # Set of units that should be excluded from waterless/fuelless computations

# https://regex101.com/r/1HEXfQ/1 for reverse direction too
COORDINATES_REGEX = r"""([A][A-N]|[A-Z])[, ]?([1-4][0-9]|[1-9])"""
UNITS_REGEX = r"""(F[1-3]|L[4-6]|R[7-9])"""
TOWNS_REGEX = '|'.join(LOCATIONS)

def put_rows(path, rows):

    if not rows:

        print(f"[WARNING] Skipping empty {path}")
        return

    with open(path, 'w', newline = '') as f:

        if isinstance(rows[0], list):
            w = csv.writer(f)

        elif isinstance(rows[0], dict):

            w = csv.DictWriter(f, fieldnames = rows[0].keys())
            w.writeheader()

        else:
            raise NotImplementedError

        for r in rows:
            w.writerow(r)

    print(f"Succesfully wrote {path}")

def load_session(args, nr):

    SESSION_PATH = pathlib.Path(args.path) / f"c3fire-{nr}"
    EXTRACTED_LOGS_PATH = SESSION_PATH / "Measurements" / "LogFiles"
    MAIN_LOG = SESSION_PATH / f"c3fire-{nr}.log"

    if args.fix:

        processed = []
        with open(MAIN_LOG, 'r') as f:
            lines = f.readlines()

        # Attempt to fix out-of-place events in faulty log files
        EVENT_REGEX = r"""<Event nr = "([0-9]*?)" time =".*?" val = ".*?" />\n"""
        events, lines_clean = [],[]
        logstart = None
        for l in lines:
            if re.fullmatch(EVENT_REGEX, l):
                events += [l]
            else:
                if l == "<Log>\n":
                    logstart = len(lines_clean)
                lines_clean += [l]

        if logstart is None:

            return "FAILED"

        lines_clean[logstart:logstart+1] = ["<Log>\n"] + events

        if lines != lines_clean:

            with open(MAIN_LOG, "w+", newline = "\r\n") as f:
                f.writelines(lines_clean)

            return "FIXED"

        else:

            return "PASSED"

        return processed

    LOG_ROOT = xml.etree.ElementTree.parse(MAIN_LOG).getroot()
    SESSION_INFO = LOG_ROOT.find("SessionInfo")
    SESSION_CON = pathlib.Path(SESSION_INFO.attrib["SessionConfigFileName"]).stem
    SESSION_SCE = pathlib.Path(SESSION_INFO.attrib["SessionScenarioFileName"]).stem
    
    if args.tranche == 1:

        SCORES_INACTION = T1S1 if "S1" in SESSION_SCE else \
                          T1S2 if "S2" in SESSION_SCE else \
                          T1S3 if "S3" in SESSION_SCE else \
                          T1R1 if "R1" in SESSION_SCE else \
                          T1R2 if "R2" in SESSION_SCE else \
                          T1R3 if "R3" in SESSION_SCE else None
 
    elif args.tranche == 2:

        SCORES_INACTION = T2S1 if "S1" in SESSION_SCE else \
                          T2S2 if "S2" in SESSION_SCE else \
                          T2S3 if "S3" in SESSION_SCE else \
                          T2R1 if "R1" in SESSION_SCE else \
                          T2R2 if "R2" in SESSION_SCE else \
                          T2R3 if "R3" in SESSION_SCE else None

    else:

        print("[WARNING] Unknown tranche number, only absolute scores will be computed")
        SCORES_INACTION = None

    print(f"\n=== SESSION {nr} - {SESSION_CON} - {SESSION_SCE} ===")

    def get_rows(path, columns):

        try:

            with open(path, newline = '') as f:

                rows = []
                for r in csv.DictReader(f, columns):
                    rows += [r]

            return rows

        except:

            print("Log files missing ! Perhaps you forgot to extract ?")
            exit()

    def atomise_rows(rows):

        new_rows = []
        i = 0
        while i + 1 < len(rows):

            while i + 1 < len(rows) and rows[i + 1]["Time"] == rows[i]["Time"]:
                i += 1

            new_rows += [rows[i]]
            i += 1

        return new_rows

    def fill_rows(rows):

        new_rows = []
        for i,c in enumerate(rows):

            if i + 1 < len(rows):
                for t in range(rows[i]["Time"] + 1, rows[i + 1]["Time"]):
                    c2 = c.copy()
                    c2["Time"] = t
                    new_rows += [c2]
                new_rows += [rows[i + 1]]

        if len(new_rows) != SIMULATION_DURATION_SECONDS + 1:

            while new_rows[-1]["Time"] != SIMULATION_DURATION_SECONDS:

                r_temp = new_rows[-1].copy()
                r_temp["Time"] += 1
                new_rows += [r_temp]

            while new_rows[0]["Time"] != 1:

                r_temp = new_rows[0].copy()
                r_temp["Time"] -= 1
                new_rows.insert(0, r_temp)

        return new_rows

    def get_id_mappings(root, tag):

        ids = {}
        for c in root.find(tag):
            ids.update({c.attrib["IdNr"] : c.attrib["IdName"]})

        return ids

    def get_object_pos(tag, typ = None, countable = False):

        objects = []
        for o in LOG_ROOT.find("ConfigFile").find("Config").find(tag):
            if ("Type" in o.attrib and o.attrib["Type"] == typ) or typ is None:

                if "Pos" not in o.attrib or (countable and "Amount" not in o.attrib):
                    print(f"[WARNING] Ill-formatted object found : {o.attrib} \n Skipping...")

                else:

                    object_coords = [int(a) for a in o.attrib["Pos"].split(',')]
                    object_amount = int(o.attrib["Amount"]) if "Amount" in o.attrib else 1
                    objects += [(object_coords, object_amount)]

        return objects

    def resourceless_times(unit_states, resource):

        resourceless_timestamps = [(i, TIME(event["time"]), event["unit_id"])
                                   for i,event in enumerate(unit_states)
                                   if float(event[resource]) == 0.0]

        is_not_rescue = lambda u : u not in RESOURCELESS_UNITS

        nosearch = {u1 : [] for u1 in UNIT_IDs.values()}

        if resource == "water":
            resourceless_times = {u1 : 0 for u1 in UNIT_IDs.values() if is_not_rescue(u1)}
            for i in RESOURCELESS_UNITS:
                if i in nosearch:
                    nosearch[i] += [(0, 1800)]
        else:
            resourceless_times = {u1 : 0 for u1 in UNIT_IDs.values()}
        resourceless_stamps = []
        for i,t,u1 in resourceless_timestamps:

            if not any([t1 <= t <= t2 for t1,t2 in nosearch[UNIT_IDs[u1]]]):

                j = i
                while j + 1 < len(unit_states) and (float(unit_states[j][resource]) != 0.0 or unit_states[j]["unit_id"] != u1 or UNIT_STATE_IDs[unit_states[j]["state_id"]].lower() != f"Refill {resource}".lower()):
                    j += 1

                if (d := ((t2 := TIME(unit_states[j]["time"])) - t)) > 0:

                    resourceless_stamps += [{"From" : t, "To" : t2, "Unit" : UNIT_IDs[u1], "Duration" : d}]
                    resourceless_times[UNIT_IDs[u1]] += d
                    nosearch[UNIT_IDs[u1]] += [(t, t2)]

        return resourceless_times, resourceless_stamps

    def people_map(people_pos):

        people = sorted(people_pos, key = lambda x : (x[0][1], x[0][0]))
        csv_rows = [MAP_SIDELENGTH * [' ']] * (people[0][0][1] - 1)
        i = 1
        total_count = 0
        while i < len(people):

            row_count = 0
            current_row = [' '] * (people[i - 1][0][0] - 1)
            current_row += [str(people[i - 1][1])]
            row_count += people[i - 1][1]

            while i < len(people) and people[i][0][1] == people[i - 1][0][1]:

                current_row += ' ' * (people[i][0][0] - people[i - 1][0][0] - 1)
                current_row += [str(people[i][1])]
                row_count += people[i][1]

                i += 1

            current_row += ' ' * (MAP_SIDELENGTH - len(current_row))
            current_row += [str(row_count)]

            csv_rows.append(current_row)

            if i < len(people) - 1:
                csv_rows += [MAP_SIDELENGTH * [' ']] * (people[i][0][1] - people[i - 1][0][1] - 1)

            total_count += row_count

            i += 1

        csv_rows += [MAP_SIDELENGTH * [' ']] * (MAP_SIDELENGTH - len(csv_rows))
        csv_rows += [(MAP_SIDELENGTH - 1) * [' '] + ["TOTAL"] + [str(total_count)]]

        return csv_rows

    # Read some mappings from session conf
    try:

        FIRE_IDs = get_id_mappings(LOG_ROOT, "FireTypeIDNrs")
        ROLE_IDs = get_id_mappings(LOG_ROOT, "RoleIDNrs")
        UNIT_IDs = get_id_mappings(LOG_ROOT, "UnitIDNrs")

        REVERSE_ROLE_IDs = {v:k for k,v in ROLE_IDs.items()}
        REVERSE_UNIT_IDs = {v:k for k,v in UNIT_IDs.items()}

        ROLE_UNITS = {}
        for r in LOG_ROOT.find("ConfigFile").find("Config").find("Roles"):
            ROLE_UNITS.update({REVERSE_ROLE_IDs[r.attrib["IDName"]] : [REVERSE_UNIT_IDs[u1] for u1 in r.attrib["ControlUnits"].replace('\'','').split(',') if u1 != '']})

        # Add your custom groups here (useful if your scenarios send automated messages, as these won't be caught by the parser and will still appear as artificial "groups" in the log files, resulting in a KeyError)
        MAILGROUPS = {"Fire Operator" : "Fire Operator", "Logistics Operator" : "Logistics Operator", "Rescue Operator" : "Rescue Operator"}
        for m in LOG_ROOT.find("ConfigFile").find("Config").find("MailGroups"):
            MAILGROUPS.update({m.attrib["IDName"] : m.attrib["Members"]})
        MAILGROUPS.update({r : r for r in ROLE_IDs.values()})

        for u2 in LOG_ROOT.find("ScenarioFile").find("Scenario").find("Events"):
            if u2.attrib["Type"] == "End":
                SIMULATION_DURATION_SECONDS = TIME(u2.attrib["Time"])

    except KeyError:
        raise xml.etree.ElementTree.ParseError

    # Raw CSV parsing
    sent_mail = get_rows(EXTRACTED_LOGS_PATH / "RoleSendMailFixRoleSQL.txt",
            ("event_id", "time_sent", "sender", "recipient", "contents"))

    read_mail = get_rows(EXTRACTED_LOGS_PATH / "role_read_mail.txt",
            ("event_id", "time_read", "recipient_id", "time_sent"))

    # http://www.c3learninglabs.com/w/index.php/C3Fire:Doc/Analysis/Log_File_Structure#Unit_State
    unit_states = get_rows(EXTRACTED_LOGS_PATH / "unit_state.txt",
            ("event_id", "time", "unit_id", "state_id", 'x', 'y', "intended_x", "intended_y",
             "water", "water_refill_speed", "water_tap_speed",
             "fuel", "fuel_refill_speed", "fuel_tap_speed",
             "people", "people_enter_time", "people_leave_time"))

    # No doc, reverse engineered
    people_states = get_rows(EXTRACTED_LOGS_PATH / "Person_In_Fire_State.txt",
            ("event_id", "time", "person_id", "unknown", 'x', 'y', "state_id"))

    # http://www.c3learninglabs.com/w/index.php/C3Fire:Doc/Analysis/Log_File_Structure#Person_Move_From_To_Unit_or_Pos_Event
    people_displacements = get_rows(EXTRACTED_LOGS_PATH / "Person_Move_From_To_Unit_or_Pos.txt",
            ("event_id", "time", "person_id", "person_type", "move_type", "unit1", "unit2", "x1", "y1", "x2", "y2"))

    fire_states = get_rows(EXTRACTED_LOGS_PATH / "fire_event.txt",
            ("event_id", "time", "state_id", 'x', 'y'))

    def parse_messages():

        # Ugly O(n^2) search but probably the only way to match read/sent messages given the ill-formatted logfiles we are fed

        messages = []
        coordination_scores = {u1 : 0 for u1 in REVERSE_UNIT_IDs}
        total_ack_lag = {r : 0 for r in REVERSE_ROLE_IDs}
        number_acks = {r : 0 for r in REVERSE_ROLE_IDs}
        for s in sent_mail:

            # Relying on C3 FixRoleSQL's weird formatting to exclude system scenario messages
            if s["sender"].replace('_', ' ').upper() != s["recipient"]:

                #Bottleneck : Textblob.correct takes a long time...
                places = re.findall(TOWNS_REGEX, str(tb.TextBlob(s["contents"]).correct()), flags = re.IGNORECASE)
                coords = re.findall(COORDINATES_REGEX, s["contents"], flags = re.IGNORECASE)
                units = re.findall(UNITS_REGEX, s["contents"], flags = re.IGNORECASE)
                coords = [(c[0].upper(), c[1].upper()) for c in coords if (c[0] + c[1]) not in units] # Excluding coordinates that look like units
                places_coords = [LOCATIONS[p.capitalize()] for p in places]
                actual_coords = [(COORDS(c[0]), int(c[1])) for c in coords]

                reacted = []
                rs = []
                mrs = 0
                mra = 0
                for r in read_mail:

                    if r["time_sent"] == s["time_sent"] \
                    and s["sender"] != ROLE_IDs[r["recipient_id"]] \
                    and s["contents"].replace(' ', '_').replace(',','') in ''.join(r[None]): # Exclude system scenario messages

                        time_sent = TIME(r["time_sent"])
                        time_read = TIME(r["time_read"])
                        time_to_ack = time_read - time_sent

                        mrs += time_to_ack
                        rs.append((ROLE_IDs[r["recipient_id"]], time_to_ack))
                        total_ack_lag[ROLE_IDs[r["recipient_id"]]] += time_to_ack
                        number_acks[ROLE_IDs[r["recipient_id"]]] += 1

                        if ROLE_IDs[r["recipient_id"]] not in [ra[0] for ra in reacted]:

                            found_reaction = False
                            for u1 in unit_states:

                                if not found_reaction \
                                and 0 <= (reaction_time := TIME(u1["time"]) - time_read) <= ACK_INTENT_TIMEOUT \
                                and u1["unit_id"] in ROLE_UNITS[r["recipient_id"]]:

                                    # Message acknolwedged within acknowledgement timeout

                                    for target in places_coords + actual_coords:

                                        if not found_reaction\
                                        and VICINITY(u1["intended_x"], u1["intended_y"], *target):

                                            # Intended location within the vicinity of target

                                            for u2 in [u for u in unit_states if u["unit_id"] == u1["unit_id"]]:

                                                if not found_reaction \
                                                and 0 <= TIME(u2["time"]) - time_read <= ACK_MOVEMENT_TIMEOUT \
                                                and VICINITY(u2['x'], u2['y'], *target) \
                                                and (u2['x'] == u2["intended_x"]) * (u2['y'] == u2["intended_y"]):

                                                    # Actual location within the vicinity of target
                                                    mra += reaction_time
                                                    coordination_scores[UNIT_IDs[u2["unit_id"]]] += 1
                                                    reacted += [(ROLE_IDs[r["recipient_id"]], reaction_time)]
                                                    found_reaction = True

                messages.append({"Time" : TIME(s["time_sent"]),
                                 "From" : s["sender"],
                                 "To" : MAILGROUPS[s["recipient"]],
                                 "Acknowledgements (Role, Time (s))" : rs,
                                 "Mean Acknowledgement Time" : mrs / len(rs) if len(rs) != 0 else "N/A",
                                 "Reactions (Role, Time (s))" : reacted,
                                 "Mean Reaction Time" : mra / len(reacted) if len(reacted) != 0 else "N/A",
                                 "Contents" : s["contents"],
                                 "Units" : units,
                                 "Locations" : list(set(places)),
                                 "Coordinates" : [c[0] + c[1] for c in coords]})

        print("Mean acknowledgement times", {r : (f"{total_ack_lag[r] / number_acks[r]:.2f}" if number_acks[r] != 0 else "N/A") for r in REVERSE_ROLE_IDs})
        print("Coordination scores", coordination_scores)

        return messages

    if not args.no_messages:

        import textblob as tb
        tb.en.spelling.update({t : 1 for t in LOCATIONS}) # Add location names to autocorrect dictionary

        print("\n--== MESSAGES ==--\n")

        put_rows(SESSION_PATH / "messages.csv", parse_messages())

    print("\n--== UNITS ==--\n")

    waterless_times, waterless_stamps = resourceless_times(unit_states, "water")
    fuelless_times, fuelless_stamps = resourceless_times(unit_states, "fuel")

    put_rows(SESSION_PATH / "waterless.csv", waterless_stamps)
    put_rows(SESSION_PATH / "fuelless.csv", fuelless_stamps)

    print("\n--== PERFORMANCE ==--\n")

    map_state = [[{v : k for k, v in FIRE_IDs.items()}["Clear"]] * MAP_SIDELENGTH] * MAP_SIDELENGTH
    people_affected_ids = set()

    people_positions = get_object_pos("Persons", "Adult", True)
    dwellings_positions = get_object_pos("Objects", "House") + get_object_pos("Objects", "Tent")
    trees_positions = get_object_pos("Objects", "Pine")
    all_positions = get_object_pos("Objects")

    _, people_amounts = zip(*people_positions)
    dwellings_coords, dwellings_amounts = zip(*dwellings_positions)
    trees_coords, trees_amounts = zip(*trees_positions)
    non_field_coords, non_field_amounts = zip(*all_positions)

    # Damage vectors: Unaffected, Saved, Lost
    people_damage = [sum(people_amounts), 0, 0]
    dwellings_damage = [sum(dwellings_amounts), 0, 0]
    trees_damage = [sum(trees_amounts), 0, 0]
    fields_damage = [MAP_SIZE - sum(non_field_amounts), 0, 0] # There are 365 non-field objects on the map, meaning this will be 40 * 40 - 365 = 1235 field tiles

    def penalise(person_id):

        if person_id in people_affected_ids:

            people_damage[1] -= 1

        else:

            people_damage[0] -= 1
            people_affected_ids.add(person_id)

    def unpenalise(person_id):

        if person_id in people_affected_ids:

            people_damage[1] += 1

    def update_damage_vector(damage_vector, data, id_mapping):

        match id_mapping[data["state_id"]]:

            case "Fire":

                if TIME(data["time"]) >= SIMULATION_DURATION_SECONDS:
                    damage_vector[2] += 1 # Burning at the end of simulation = lost

                if "person_id" in data:

                    penalise(data["person_id"])

                else:

                    damage_vector[0] -= 1

            case "ClosedOut":

                if "person_id" in data:

                    unpenalise(data["person_id"])

                else:

                    damage_vector[1] += 1

            case "BurnedOut":

                damage_vector[2] += 1

    scores = []
    for state in sorted(people_displacements + people_states + fire_states, key = lambda s : s["time"]):

        # Dealing with people displacement
        if "move_type" in state:

            x1_state = FIRE_IDs[map_state[int(state["x1"]) - 1][int(state["y1"]) - 1]]

            #http://www.c3learninglabs.com/w/index.php/C3Fire:Doc/Analysis/Log_File_Structure#Person_Move_From_To_Unit_or_Pos_Event
            match state["move_type"]:

                # Rescue unit picks up people
                case '1':

                    if x1_state == "Fire":

                        unpenalise(state["person_id"])

                # Rescue unit drops off people
                case '2':

                    if x1_state == "Fire":

                        penalise(state["person_id"])

                # Manager moves people
                case '5':

                    x2_state = FIRE_IDs[map_state[int(state["x2"]) - 1][int(state["y2"]) - 1]]

                    if x1_state == "Fire" and x2_state in SAFE_TILESTATES:

                        unpenalise(state["person_id"])

                    elif x1_state in SAFE_TILESTATES and x2_state == "Fire":

                        penalise(state["person_id"])

        # Dealing with a tile that has people on it
        elif "person_id" in state:

            update_damage_vector(people_damage, state, FIRE_IDs)

        # Dealing with a non-people tile
        else:

            fire_location = [int(state['x']), int(state['y'])]
            map_state[fire_location[0] - 1][fire_location[1] - 1] = state["state_id"]

            if fire_location in dwellings_coords:
                update_damage_vector(dwellings_damage, state, FIRE_IDs)

            if fire_location in trees_coords:
                update_damage_vector(trees_damage, state, FIRE_IDs)

            elif fire_location not in non_field_coords:
                update_damage_vector(fields_damage, state, FIRE_IDs)

        suffix = "worst" if args.inaction else "team"
        scores += [{"Time" : TIME(state["time"]),
                    f"People_{suffix}" : people_damage[0] + people_damage[1],
                    f"Dwellings_{suffix}" : dwellings_damage[0] + dwellings_damage[1],
                    f"Trees_{suffix}" : trees_damage[0] + trees_damage[1],
                    f"Fields_{suffix}" : fields_damage[0] + fields_damage[1]}]

    scores = fill_rows(atomise_rows(scores))

    if args.inaction:

        with open(pathlib.Path(__file__).parent / f"inaction-{SESSION_SCE}.py", "w+") as f:
            f.write(f"{SESSION_SCE}_INACTION = {repr(scores)}\n\n")

    else:

        perf_absolute = lambda d : 10 * d["People_team"] + 5 * d["Dwellings_team"] + 2 * d["Trees_team"] + d["Fields_team"]
        perf_absolute_worst = lambda d : 10 * d["People_worst"] + 5 * d["Dwellings_worst"] + 2 * d["Trees_worst"] + d["Fields_worst"]

        if SCORES_INACTION is None:

            print("[WARNING] No worst case scores are available for this scenario. Printing only absolute team scores instead...")

            abscores_team = []
            for s in scores:

                absteam = perf_absolute(s)
                abscores_team += [absteam]
                s |= {"Absolute Team Performance" : absteam}

        else:

            scores = [{"People_worst" : SCORES_INACTION[s["Time"] - 1]["People_worst"],
                    "Dwellings_worst" : SCORES_INACTION[s["Time"] - 1]["Dwellings_worst"],
                    "Trees_worst" : SCORES_INACTION[s["Time"] - 1]["Trees_worst"],
                    "Fields_worst" : SCORES_INACTION[s["Time"] - 1]["Fields_worst"]} | s for s in scores]

            abscores_team = []
            abscores_worst = []
            relscores = []
            for s in scores:

                absteam = perf_absolute(s)
                absworst = perf_absolute_worst(s)
                relteam = 100 * ((absteam / absworst) - 1)

                abscores_team += [absteam]
                abscores_worst += [absworst]
                relscores += [relteam]
                s |= {"Absolute Worst Performance" : absworst, "Absolute Team Performance" : absteam, "Relative Team Performance" : relteam}

        put_rows(SESSION_PATH / "scores.csv", scores)

        put_rows(SESSION_PATH / "people_positions.csv", people_map(people_positions))

        if args.graph:

            import matplotlib.pyplot as plt

            datapoints = range(1, len(scores) + 1)

            plt.figure("absolute")
            plt.plot(datapoints, abscores_team, label = "Team performance")
            plt.xlabel("Time (seconds)")
            plt.ylabel("Team score")
            plt.title("Absolute team performance")

            if SCORES_INACTION is not None:

                plt.plot(datapoints, abscores_worst, label = "Inaction performance")
                plt.ylabel("Scores")
                plt.title("Performance comparison")

                plt.figure("relative")
                plt.plot(datapoints, relscores)
                plt.xlabel("Time (seconds)")
                plt.ylabel("Relative score")
                plt.title("Relative team performance")
                plt.savefig(SESSION_PATH / "scores_relative.svg")
                plt.clf()

            plt.figure("absolute")
            plt.legend()
            plt.savefig(SESSION_PATH / "scores.svg")
            plt.clf()

        return scores[-1]["Relative Team Performance"] if "Relative Team Performance" in scores[-1] else scores[-1]["Absolute Team Performance"]


if __name__ == "__main__":

    parser = argparse.ArgumentParser(prog = "C3Parser", description = "A C3Fire log file parser")
    parser.add_argument("-f", "--fix", action = "store_true")
    parser.add_argument("-n", "--no-messages", action = "store_true")
    parser.add_argument("-g", "--graph", action = "store_true")
    parser.add_argument("-i", "--inaction", action = "store_true")
    parser.add_argument("-t", "--tranche", type = int)
    parser.add_argument("path", type = str)
    parser.add_argument("range_start", type = int)
    parser.add_argument("range_end", type = int)
    args = parser.parse_args()

    if not args.tranche and not (args.inaction or args.fix):
        raise ValueError("Data tranche must be specified with -t/--tranche <number> !")

    range_scores = []
    for session_number in range(args.range_start, args.range_end + 1):

        try:

            range_scores += [{"Session n°" : session_number, "Outcome" : load_session(args, session_number)}]

        except FileNotFoundError:

            print(f"[WARNING] Session number {session_number} not found. Skipping...")

        except xml.etree.ElementTree.ParseError:

            range_scores += [{"Session n°" : session_number, "Outcome" : "FAULTY LOGS"}]
            print(f"Faulty log file n°{session_number}, skipping...")

    put_rows(pathlib.Path(args.path) / "range.csv", range_scores)